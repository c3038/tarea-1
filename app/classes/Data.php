<?php

namespace App\Classes;

abstract class Data
{
   abstract public function save(): string;

   abstract public function new(): string;

   private function generate_string($strength = 7): string
   {
      $chars = 'abcdefghijklmnopqrstuvwxyz';
      $chars_length = strlen($chars);
      $random_string = '';

      for ($i = 0; $i < $strength; $i++) {
         $random_character = $chars[mt_rand(0, $chars_length - 1)];
         $random_string .= $random_character;
      }

      return ucfirst($random_string);
   }

   public function getName(): string
   {
      return $this->generate_string(random_int(2, 10));
   }
}
