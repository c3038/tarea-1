<?php

namespace App\Classes;

class FormData
{
   private array $data = [];

   public function __get($key)
   {
      if (array_key_exists($key, $this->data)) {
         return $this->data[$key];
      }
      return null;
   }

   public function __set($key, $value)
   {
      $this->data[$key] = $value;
   }
}
