<?php

namespace App\Classes;

class SortArray
{
   private array $array;
   public function __construct($array)
   {
      $this->array = $array;
   }

   private function customMultiSort($array, $field): array
   {
      $sortArr = array_column($array, $field);
      array_multisort($sortArr, $array);

      return $array;
   }

   public function sort(): array
   {
      foreach ($this->array as $key => &$value) {
         foreach ($value as $item => $items) {
            $value[$item] = $this->customMultiSort($items, 'PRICE');
         }
      }

      return $this->array;
   }
}
