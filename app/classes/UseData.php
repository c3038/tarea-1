<?php

namespace App\Classes;

class UseData extends Data
{

   public function save(): string
   {
      return 'Работает метод save()!';
   }

   public function new(): string
   {
      return 'Работает метод new()!';
   }

   public function getName(): string
   {
      return parent::getName() . ' Здорово!';
   }
}
