<?php
return [
   'SKLAD1' => [
      'EDA' => [
         'TOVAR1' => [
            'NAME' => '....',
            'PRICE' => 1235
         ],
         'TOVAR2' => [
            'NAME' => '....',
            'PRICE' => 1234
         ],
         'TOVAR3' => [
            'NAME' => '....',
            'PRICE' => 1236
         ],
      ],
      'NAPITKI' => [
         'TOVAR55' => [
            'NAME' => '....',
            'PRICE' => 1238
         ],
         'TOVAR54' => [
            'NAME' => '....',
            'PRICE' => 1234
         ],
      ],
   ],
   'SKLAD2' => [
      'EDA' => [
         'TOVAR66' => [
            'NAME' => '....',
            'PRICE' => 1231
         ],
         'TOVAR67' => [
            'NAME' => '....',
            'PRICE' => 1234
         ],
      ],
      'NAPITKI' => [
         'TOVAR77' => [
            'NAME' => '....',
            'PRICE' => 1240
         ],
         'TOVAR78' => [
            'NAME' => '....',
            'PRICE' => 1234
         ],
      ],
   ],
];
