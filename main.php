<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\Classes\FormData;
use App\Classes\SortArray;
use App\Classes\UseData;

$data = new FormData();

if (isset($_POST['name']) && isset($_POST['surname'])) {
   $data->name = $_POST['name'];
   $data->surname = $_POST['surname'];
}
$config_data = file_get_contents(__DIR__ . '/data/db_config.json');
$config = json_decode($config_data, true);

$dsn = "mysql:host=" . $config['host'] . ";port=" . $config['port'] . ";dbname=" . $config['dbname'];

try {
   $db = new PDO($dsn, $config['username'], $config['password'], $config['options']);
} catch (\PDOException $exeption) {
   echo $exeption->getMessage();
   die();
}

/* ============= Задание 1 ============= */
echo "<h1>{$data->name} {$data->surname}</h1>";

/* ============= Задание 2 ============= */
$mok = require __DIR__ . '/data/mock.php';

$mokSort = new SortArray($mok);
?>
<pre>
   <?php print_r($mokSort->sort()); ?>
</pre>
<?php

/* ============= Задание 3 ============= */
$dataFunction = new UseData();

echo "<h2>{$dataFunction->getName()}</h2>";
echo "<h2>{$dataFunction->save()}</h2>";
echo "<h2>{$dataFunction->new()}</h2>";

/* ============= Задание 4 ============= */
$query = $db->prepare("INSERT INTO `users` (`name`, `sername`) VALUES (:name, :sername)");
$query->execute([
   'name' => $data->name,
   'sername' => $data->surname
]);
